let myNum = 8;
let getCube = myNum ** 3;

console.log(`The cube of ${myNum} is ${getCube}.`);

//  

let address = ["308 Negra Arroyo Lane", "Alburqurque", "New Mexico", "87111"];
let [streetAddress, city, state, zipCode] = address;

console.log(`I live in ${streetAddress} ${city}, ${state} ${zipCode}.`);

const animal = {
    name: "Yuki",
    species: "dog",
    weight: "6 kg",
    length: "40 cm"
}
const {name, species, sex, weight, length} = animal;

console.log(`${name} is a ${species}. She weighs ${weight} with a measurement of ${length}.`);

const myNumbers = [1,2,3,4,5,6,7,8,9];

myNumbers.forEach((number) => console.log(number));

const reduceNumber = myNumbers.reduce((sum, cVal) => sum + cVal, 0);
console.log(reduceNumber);

class Dog {
    constructor(name, age, breed){
        this.name = name;
        this.age = age;
        this.breed = breed;
    }
}

const empoy = new Dog("Empoy", 1, "French Bulldog");
console.log(empoy);